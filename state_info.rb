STATES = {
  OR: {
    name: 'Oregon',
    cities: ['Salem', 'Portland'],
    tax: 0
    },
  FL: {
    name: 'Florida',
    cities: ['Tallahassee', 'Jacksonville', 'Miami'],
    tax: 7.5
    },
  CA: {
    name: 'California',
    cities: ['Sacramento', 'Los Angeles'],
    tax: 10
    },
  NY: {
    name: 'New York',
    cities: ['Albany', 'New York'],
    tax: 8.875
    },
  MI: {
    name: 'Michigan',
    cities: ['Lansing', 'Detroit'],
    tax: 6
    },
  WA: {
    name: 'Washington',
    cities: ['Olympia', 'Seattle'],
    tax: 9.6
    },
  UT: {
    name: 'Utah',
    cities: ['Salt Lake City', 'Provo'],
    tax: 8.35
    }
}

def describe_state(state_code)
  sc = state_code.to_sym
  "#{sc} is for #{STATES[sc][:name]}. It has #{STATES[sc][:cities].size} major cities: #{STATES[sc][:cities].join(", ")}" if STATES.has_key?(sc)
end

def calculate_tax(state_code, amount)
  rate = STATES[state_code.to_sym][:tax]
  (amount * rate/100).round(2)
end

def find_state_for_city(city)
  STATES.select { |k,v| v[:cities].include?(city) }.keys
end


puts describe_state('CA')
puts describe_state('VI')
puts calculate_tax('FL', 50)
puts find_state_for_city('Seattle')
puts find_state_for_city('Hahahahaha')
